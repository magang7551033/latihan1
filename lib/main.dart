import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // Widget dasar aplikasi dengan title dan theme yang sudah ditentukan
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Latihan 1'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title; // Variabel yang menyimpan title dari aplikasi

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Inisialisasi counter dan list warna
  int _counter = 0;
  List<Color> colorList = [Colors.black, Colors.purple, Colors.orange];

  void _incrementCounter() {
    setState(() {
      // Counter yang akan reset ke 0 jika sudah sampai nilai 2. Counter ini
      // Akan digunakan untuk memilih warna pada kotak
      if (_counter == 2) {
        _counter = 0;
      } else {
        _counter++;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(10),
              // Warna kotak diambil dari list warna dengan index counter
              // Variabel counter akan berubah saat tombol Ganti Warna ditekan
              color: colorList[_counter],
              width: 200,
              height: 200,
            ),
            // Tombol "Ganti Warna"
            ElevatedButton(
                onPressed: _incrementCounter, child: const Text("Ganti Warna"))
          ],
        ),
      ),
    );
  }
}
